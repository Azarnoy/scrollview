package com.dmitriy.azarenko.scrollviewandviewincode;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;


public class BlankFragment extends Fragment {

    public static final String keyForInstanse = "Key";

    private ArrayList<String> editTextList = new ArrayList<String>();

    LinearLayout ll;
    FrameLayout Fl;
    Button check;



    public BlankFragment() {

    }

    public static BlankFragment newInstance() {
        BlankFragment fragment = new BlankFragment();

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_blank, null);



        ll = (LinearLayout) view.findViewById(R.id.LineLi);
        Fl = (FrameLayout) view.findViewById(R.id.Frame_lay);
        check = (Button) view.findViewById(R.id.button2);

        LinearLayout.LayoutParams lEditParams = new LinearLayout.LayoutParams(
                300, LinearLayout.LayoutParams.WRAP_CONTENT);


        if (savedInstanceState !=null){
            editTextList = savedInstanceState.getStringArrayList("Key");
        }else{


        }






        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i < editTextList.size(); i++) {

                    if (editTextList.get(i) != null){
                        Toast.makeText(getContext(), "full",Toast.LENGTH_SHORT).show();

                    }else{
                        Toast.makeText(getContext(), "null",Toast.LENGTH_SHORT).show();
                    }



                }


            }
        });


        for (int i = 0; i < 20; i++) {
            EditText editTxt = new EditText(this.getContext()); //почему GetContext
            editTxt.setLayoutParams(lEditParams);
            editTextList.add(i, String.valueOf(editTxt));
            ll.addView(editTxt);


        }








        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putStringArrayList("Key", editTextList);
    }


}
